# description	: OpenGL compatible 3D graphics library
# maintainer	: Venom Linux Team, venomlinux at disroot dot org
# homepage	: https://www.mesa3d.org/
# depends	: elfutils-32 llvm-32 mesa libglvnd-32 libxdamage-32 libxfixes-32 libxxf86vm-32 libxv-32 libxvmc-32 libxrandr-32 
# depends	: libxshmfence-32 glslang wayland-32 zlib-32 libdrm-32 spirv-llvm-translator-32 clang-32
# optional	: libva libvdpau-32 wayland-protocols

name=mesa-32
version=25.0.1
release=1
source="https://mesa.freedesktop.org/archive/${name%-*}-$version.tar.xz
	crossfile"

build() {
	cd ${name%-*}-$version 
	
	export BINDGEN_EXTRA_CLANG_ARGS="-m32"
	export PKG_CONFIG_LIBDIR='/usr/lib32/pkgconfig:/usr/share/pkgconfig'

	scratch isinstalled vulkan-icd-loader-32 && OPT_MESA_GALLIUM='zink,'
	scratch isinstalled wayland-protocols && OPT_MESA_PLATFORMS='wayland,x11' ||  OPT_MESA_PLATFORMS='x11'

  	venom-meson build \
		--sysconfdir=/etc \
		--libdir=/usr/lib32 \
		--cross-file=$SRC/crossfile \
		--wrap-mode=default \
		-D b_lto=false \
		-D valgrind=disabled \
		-D egl=enabled \
		-D llvm=enabled \
		-D shared-llvm=enabled \
		-D gbm=enabled \
		-D gles1=disabled \
		-D gles2=enabled \
		-D glx=dri \
		-D gallium-xa=enabled \
		-D gallium-drivers=${OPT_MESA_GALLIUM}crocus,iris,nouveau,r300,r600,radeonsi,svga,swrast,virgl,i915 \
 		-D gallium-opencl=icd \
 		-D gallium-rusticl=true \
 		-D shared-glapi=enabled \
		-D platforms=${OPT_MESA_PLATFORMS} \
		-D shared-glapi=enabled \
		-D vulkan-drivers=auto \
		-D vulkan-layers=device-select,intel-nullhw,overlay \
		-D video-codecs=vc1dec,h264dec,h264enc,h265dec,h265enc \
		-D glvnd=enabled 
	meson compile -C build
	DESTDIR=$PKG meson install --no-rebuild -C build

	# indirect rendering symlink
	ln -s libGLX_mesa.so.0 $PKG/usr/lib32/libGLX_indirect.so.0
  
	rm -rf $PKG/usr/include $PKG/usr/share/glvnd $PKG/etc/OpenCL
	rm -f $PKG/usr/share/drirc.d/00-mesa-defaults.conf
	rm -f $PKG/usr/share/drirc.d/00-radv-defaults.conf

	
	[ -f "/usr/bin/mesa-overlay-control.py" ] && \
		rm $PKG/usr/bin/mesa-overlay-control.py

	[ -f "/usr/share/vulkan/implicit_layer.d/VkLayer_MESA_device_select.json" ] && \
		rm $PKG/usr/share/vulkan/implicit_layer.d/VkLayer_MESA_device_select.json

	[ -f "/usr/share/vulkan/explicit_layer.d/VkLayer_INTEL_nullhw.json" ] && \
		rm $PKG/usr/share/vulkan/explicit_layer.d/VkLayer_INTEL_nullhw.json

	[ -f "/usr/share/vulkan/explicit_layer.d/VkLayer_MESA_overlay.json" ] && \
		rm $PKG/usr/share/vulkan/explicit_layer.d/VkLayer_MESA_overlay.json
}
